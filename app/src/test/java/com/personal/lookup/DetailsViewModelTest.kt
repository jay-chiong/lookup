package com.personal.lookup

import android.app.Application
import android.content.Context
import com.personal.lookup.details.DetailsViewModel
import junit.framework.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class DetailsViewModelTest
{
    private val context = Mockito.mock(Context::class.java)

    private var application = Mockito.mock(Application::class.java)

    private lateinit var _detailsViewModel: DetailsViewModel

    @Before
    fun setupSearchResultViewModel()
    {
        MockitoAnnotations.openMocks(this)
    }

    @Test
    fun testConstructor()
    {
        _detailsViewModel = DetailsViewModel(application, context)
        assertNotNull(_detailsViewModel)
    }
}