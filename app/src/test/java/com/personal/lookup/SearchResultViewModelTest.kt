package com.personal.lookup

import android.app.Application
import android.content.Context
import com.personal.lookup.search.SearchResultViewModel
import junit.framework.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class SearchResultViewModelTest
{
    private val context = Mockito.mock(Context::class.java)

    private var application = Mockito.mock(Application::class.java)

    private lateinit var _searchResultViewModel: SearchResultViewModel

    @Before
    fun setupSearchResultViewModel()
    {
        MockitoAnnotations.openMocks(this)
    }

    @Test
    fun testConstructor()
    {
        _searchResultViewModel = SearchResultViewModel(application, context)
        assertNotNull(_searchResultViewModel)
    }

    @Test
    fun testGetSuggestionsNotNullCursor()
    {
        _searchResultViewModel = SearchResultViewModel(application, context)
        assertNotNull(_searchResultViewModel.getSuggestions())
    }
}