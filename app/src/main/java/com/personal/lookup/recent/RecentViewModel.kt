package com.personal.lookup.recent

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.personal.lookup.common.SingleLiveEvent

class RecentViewModel(application: Application) : AndroidViewModel(application)
{
    val onSearchClick = SingleLiveEvent<Void>()

    fun onSearchClicked()
    {
        onSearchClick.call()
    }
}