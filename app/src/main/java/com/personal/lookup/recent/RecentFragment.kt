package com.personal.lookup.recent

import android.app.Application
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.personal.lookup.R
import com.personal.lookup.common.ViewModelFactory
import com.personal.lookup.databinding.FragmentRecentBinding

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class RecentFragment : Fragment()
{
    private lateinit var _binding: FragmentRecentBinding
    private lateinit var _recentViewModel: RecentViewModel
    private lateinit var _activity: FragmentActivity

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root: View = inflater.inflate(R.layout.fragment_recent, container, false)

        _binding = FragmentRecentBinding.bind( root )
        _activity = activity as FragmentActivity

        setupViewModel()

        return _binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.findViewById<TextView>(R.id.toolbar_tv)?.visibility = View.VISIBLE
    }

    private fun setupViewModel()
    {
        _recentViewModel = getRecentViewModel(_activity.application)

        _binding.viewModel = _recentViewModel

        _recentViewModel.onSearchClick.observe(this, {
            findNavController().navigate(R.id.action_RecentFragment_to_SearchResultFragment)
        })
    }

    private fun getRecentViewModel(application: Application) : RecentViewModel
    {
        val factory = ViewModelFactory(application, _activity).getInstance()

        return ViewModelProvider(this, factory).get(RecentViewModel::class.java)
    }

}