package com.personal.lookup.search

import android.app.Application
import android.app.SearchManager
import android.database.Cursor
import android.os.Bundle
import android.view.*
import android.widget.AdapterView
import android.widget.AutoCompleteTextView
import android.widget.TextView
import androidx.appcompat.widget.SearchView
import androidx.cursoradapter.widget.CursorAdapter
import androidx.cursoradapter.widget.SimpleCursorAdapter
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.personal.lookup.R
import com.personal.lookup.common.ViewModelFactory
import com.personal.lookup.databinding.FragmentSearchResultBinding
import com.personal.lookup.repository.model.Movies
import com.personal.lookup.repository.model.Search
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Fragment class for showing the search results
 */
class SearchResultFragment : Fragment()
{
    private lateinit var _binding: FragmentSearchResultBinding
    private lateinit var _searchResultViewModel: SearchResultViewModel
    private lateinit var _activity: FragmentActivity
    private lateinit var _searchResultAdapter: SearchResultAdapter

    private var searchResult = ArrayList<Movies>()

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root: View = inflater.inflate(R.layout.fragment_search_result, container, false)

        _binding = FragmentSearchResultBinding.bind( root )
        _activity = activity as FragmentActivity

        setupViewModel()

        return _binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater)
    {
        inflater.inflate(R.menu.menu_main, menu)

        val searchItem = menu.findItem(R.id.search)
        val searchView = searchItem.actionView as SearchView

        searchView.isIconified = false

        // Minimum of 3 characters is required from the API for search or else the API will respond:
        // {
        //    "Response": "False",
        //    "Error": "Too many results."
        //}
        // thus setting it to 3 for auto request
        searchView.findViewById<AutoCompleteTextView>(R.id.search_src_text).threshold = 3
        searchView.maxWidth = 1200 // change to screen width

        val from = arrayOf(SearchManager.SUGGEST_COLUMN_TEXT_1)
        val to = intArrayOf(R.id.suggestion_tv)
        val cursorAdapter = SimpleCursorAdapter(context, R.layout.item_suggestion, null, from, to, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)

        searchView.suggestionsAdapter = cursorAdapter

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener
        {
            override fun onQueryTextSubmit(query: String?): Boolean
            {
                if ((query as String).length > 2)
                {
                    doRequest(query, 1, true, null)
                }

                return false
            }

            override fun onQueryTextChange(query: String?): Boolean
            {
                if ((query as String).length > 2)
                {
                    doRequest(query, 1, false, cursorAdapter)
                }

                return true
            }
        })

        searchView.setOnSuggestionListener(object: SearchView.OnSuggestionListener
        {
            override fun onSuggestionSelect(position: Int): Boolean
            {
                return false
            }

            override fun onSuggestionClick(position: Int): Boolean
            {
                val cursor = searchView.suggestionsAdapter.getItem(position) as Cursor
                val selection = cursor.getString(cursor.getColumnIndex(SearchManager.SUGGEST_COLUMN_TEXT_1))
                searchView.setQuery(selection, false)

                // Do something with selection
                // TODO: query details
                return true
            }

        })

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)

        activity?.findViewById<TextView>(R.id.toolbar_tv)?.visibility = View.GONE

        setHasOptionsMenu(true)
    }

    private fun setupViewModel()
    {
        _searchResultViewModel = getSearchResultViewModel(_activity.application)

        _binding.viewModel = _searchResultViewModel

        _searchResultAdapter = SearchResultAdapter(searchResult, _activity)
        _binding.resultsRv.adapter = _searchResultAdapter
        _binding.resultsRv.setOnItemClickListener { _, _, position, _ ->
            val item = _binding.resultsRv.adapter.getItem(position) as Movies
            val bundle = Bundle()
            bundle.putString("title", item.Title)
            bundle.putString("year", item.Year)
            findNavController().navigate(R.id.action_SearchResultFragment_to_detailsFragment, bundle)
        }
    }

    private fun getSearchResultViewModel(application: Application) : SearchResultViewModel
    {
        val factory = ViewModelFactory(application, _activity).getInstance()

        return ViewModelProvider(this, factory).get(SearchResultViewModel::class.java)
    }

    private fun doRequest(query: String, page: Int, isForSearch: Boolean, cursorAdapter: SimpleCursorAdapter?)
    {
        if (query.length > 2)
        {
            if (isForSearch)
                _searchResultViewModel.isLoading.set(true)

            _searchResultViewModel.getMoviesByTitle(query, page, isForSearch, object : Callback<Search>{
                override fun onResponse(
                    call: Call<Search>,
                    response: Response<Search>
                )
                {
                    if (response.isSuccessful)
                    {
                        if (cursorAdapter != null && response.body()?.totalResults!! > 0)
                        {
                            val cursor = _searchResultViewModel.getSuggestions()

                            cursorAdapter?.changeCursor(cursor)
                        }
                        else
                        {
                            cursorAdapter?.cursor?.close()

                            if (page == 1)
                                searchResult.clear()

                            response.body()?.Search?.let { searchResult.addAll(it) }
                            _searchResultAdapter.notifyDataSetChanged()
                            _searchResultViewModel.isLoading.set(false)
                        }
                    }
                    else
                    {

                    }
                }

                override fun onFailure(call: Call<Search>, t: Throwable)
                {
                    // TODO
                }

            })
        }
    }
}