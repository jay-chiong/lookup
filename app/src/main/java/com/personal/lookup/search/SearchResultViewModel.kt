package com.personal.lookup.search

import android.app.Application
import android.app.SearchManager
import android.content.Context
import android.database.MatrixCursor
import android.provider.BaseColumns
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.AndroidViewModel
import com.personal.lookup.repository.model.Movies
import com.personal.lookup.repository.model.Search
import com.personal.lookup.repository.source.ServerDataSource
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Handles the request and process of SearchResultFragment
 */
class SearchResultViewModel(
        application: Application,
        context: Context) : AndroidViewModel(application)
{
    private val service = ServerDataSource.create(context)
    private var suggestionMovies = ArrayList<String>()

    private lateinit var call: Call<Search>

    val isNoResult = ObservableBoolean(false)
    val isLoading = ObservableBoolean(false)

    /**
     * Trigger a request to get all movies that contains the search word
     * @param title The title of the movie
     * @param page The page number to be requested
     * @param isForSearch A boolean value to check if the request is for suggestion or search result
     * @param callback A callback to notify the fragment that the request is done
     */
    fun getMoviesByTitle(title: String, page: Int, isForSearch: Boolean, callback: Callback<Search>)
    {
        if (this::call.isInitialized)
        {
            call.cancel()
        }

        call = service.getMovies(title, page)
        call.enqueue(object : Callback<Search>
        {
            override fun onResponse(call: Call<Search>, response: Response<Search>)
            {
                if (response.isSuccessful)
                {
                    if (response.body()?.Search?.isNotEmpty()!!)
                    {
                        val search = response.body()?.Search as ArrayList<Movies>

                        if (!isForSearch)
                        {
                            suggestionMovies.clear()

                            for(movie in search)
                            {
                                suggestionMovies.add(movie.Title)
                            }
                        }
                        else
                        {
                            isNoResult.set(false)
                        }
                    }
                    else
                    {
                        if (isForSearch)
                            isNoResult.set(true)
                    }

                    callback.onResponse(call, response)
                }
            }

            override fun onFailure(call: Call<Search>, t: Throwable)
            {
                callback.onFailure(call, t)
            }

        })
    }

    /**
     * Adding the result title of the api to the cursor for the suggestion list
     */
    fun getSuggestions() : MatrixCursor
    {
        val cursor = MatrixCursor(arrayOf(BaseColumns._ID, SearchManager.SUGGEST_COLUMN_TEXT_1))

        if (suggestionMovies.size > 0)
        {
            suggestionMovies.forEachIndexed { index, suggestion ->
                cursor.addRow(arrayOf(index, suggestion))
            }
        }

        return cursor
    }
}