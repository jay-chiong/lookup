package com.personal.lookup.search

import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.personal.lookup.R
import com.personal.lookup.repository.model.Movies

/**
 * Handles the display of the movies
 */
class SearchResultAdapter(
    private val result: ArrayList<Movies>,
    private val activity: FragmentActivity
) : BaseAdapter()
{
    override fun getCount(): Int
    {
        return result.size
    }

    override fun getItem(position: Int): Any
    {
        return result[position]
    }

    override fun getItemId(position: Int): Long
    {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View
    {
        var view = convertView
        var holder: ViewHolder

        if (null == view)
        {
            view = View.inflate(activity, R.layout.item_search_result,null)
            holder = ViewHolder()

            holder.imageView = view.findViewById(R.id.movie_iv)
            holder.textView = view.findViewById(R.id.title_tv)

            view.tag = holder
        }
        else
        {
            holder = view.tag as ViewHolder
        }

        if (holder != null)
        {
            Glide.with(activity)
                .load(result[position].Poster)
                .dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
                .into(holder.imageView)
        }

        holder?.textView?.text = result[position].Title

        return view as View
    }

    inner class ViewHolder()
    {
        lateinit var imageView: ImageView
        lateinit var textView: TextView
    }

}