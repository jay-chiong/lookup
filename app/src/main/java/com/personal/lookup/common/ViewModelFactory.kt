package com.personal.lookup.common

import android.app.Application
import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.personal.lookup.details.DetailsViewModel
import com.personal.lookup.recent.RecentViewModel
import com.personal.lookup.search.SearchResultViewModel

/**
 * Manage all viewModels
 */
class ViewModelFactory(
    private val application: Application,
    private val context: Context) : ViewModelProvider.NewInstanceFactory()
{
    private lateinit var INSTANCE: ViewModelFactory

    fun getInstance() : ViewModelFactory
    {
        if ( !this::INSTANCE.isInitialized )
        {
            INSTANCE = ViewModelFactory(application, context)
        }

        return INSTANCE
    }

    override fun <T : ViewModel?> create(modelClass: Class<T>): T
    {
        return when {
            modelClass.isAssignableFrom(RecentViewModel::class.java) -> {
                RecentViewModel(application) as T
            }
            modelClass.isAssignableFrom(SearchResultViewModel::class.java) -> {
                SearchResultViewModel(application, context) as T
            }
            modelClass.isAssignableFrom(DetailsViewModel::class.java) -> {
                DetailsViewModel(application, context) as T
            }
            else -> throw IllegalArgumentException("Unknown ViewModel class: " + modelClass.name)
        }

    }
}