package com.personal.lookup.repository.model

data class Search(
        var Search: List<Movies>? = emptyList(),
        val totalResults: Int = 0
)
