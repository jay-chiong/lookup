package com.personal.lookup.repository.source

import android.content.Context
import androidx.annotation.NonNull
import com.personal.lookup.repository.model.MovieDetails
import com.personal.lookup.repository.model.Movies
import com.personal.lookup.repository.model.Search
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface ServerDataSource
{
    @GET("/?apikey=442f9e3c")
    fun getMovies(
            @Query("s") @NonNull title: String,
            @Query("page") page: Int) : Call<Search>

    @GET("/?apikey=442f9e3c")
    fun getMovieDetailsByTitleAndYear(
        @Query("t") @NonNull title: String,
        @Query("y",) @NonNull year: String) : Call<MovieDetails>


    companion object
    {
        var BASE_URL = "http://www.omdbapi.com"

        private const val cacheSize = (10 * 1024 * 1024).toLong()

        fun create(context: Context) : ServerDataSource
        {
            val myCache = Cache(context.cacheDir, cacheSize)

            val okHttpClient = OkHttpClient.Builder()
                .cache(myCache)
                .build()

            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .build()
            return retrofit.create(ServerDataSource::class.java)

        }
    }
}