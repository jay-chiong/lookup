package com.personal.lookup.repository.model

data class Rating(
    val Source: String,
    val Value: String
)
