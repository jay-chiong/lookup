package com.personal.lookup.details

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import com.personal.lookup.R
import com.personal.lookup.common.ViewModelFactory
import com.personal.lookup.databinding.FragmentDetailsBinding

/**
 * Fragment class for showing the other information of the selected result
 */
class DetailsFragment : Fragment()
{
    private lateinit var _binding: FragmentDetailsBinding
    private lateinit var _detailsViewModel: DetailsViewModel
    private lateinit var _activity: FragmentActivity

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View?
    {
        val root: View = inflater.inflate(R.layout.fragment_details, container, false)

        _binding = FragmentDetailsBinding.bind( root )
        _activity = activity as FragmentActivity

        setupViewModel()

        return _binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?)
    {
        super.onViewCreated(view, savedInstanceState)

        val imageView = _binding.detailsIv as ImageView

        val title = arguments?.getString("title") as String
        val year = arguments?.getString("year") as String

        _detailsViewModel.getMovieDetails(title, year, imageView, _activity)
    }

    private fun setupViewModel()
    {
        _detailsViewModel = getDetailsViewModel(_activity.application)

        _binding.viewModel = _detailsViewModel
    }

    private fun getDetailsViewModel(application: Application) : DetailsViewModel
    {
        val factory = ViewModelFactory(application, _activity).getInstance()

        return ViewModelProvider(this, factory).get(DetailsViewModel::class.java)
    }
}