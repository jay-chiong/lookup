package com.personal.lookup.details

import android.app.Application
import android.content.Context
import android.widget.ImageView
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import com.bumptech.glide.Glide
import com.personal.lookup.repository.model.MovieDetails
import com.personal.lookup.repository.source.ServerDataSource
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Handles the request and process of DetailsFragment
 */
class DetailsViewModel(
    application: Application,
    context: Context) : AndroidViewModel(application)
{
    private val service = ServerDataSource.create(context)

    val isLoading = ObservableBoolean(false)

    var title = ObservableField<String>()
    var desc = ObservableField<String>()
    var year = ObservableField<String>()
    var rating = ObservableField<String>()

    /**
     * Trigger a request to get all the details of the movie
     * @param keyTitle The title of the movie
     * @param keyYear The year of the movie
     * @param imageView The instance of an imageView on where the image will be displayed
     * @param context The context of the activity
     */
    fun getMovieDetails(keyTitle: String, keyYear: String, imageView: ImageView, context: Context)
    {
        isLoading.set(true)

        val call = service.getMovieDetailsByTitleAndYear(keyTitle, keyYear)
        call.enqueue(object : Callback<MovieDetails>
        {
            override fun onResponse(call: Call<MovieDetails>, response: Response<MovieDetails>)
            {
                if (response.isSuccessful)
                {
                    Glide.with(context).load(response.body()?.Poster).into(imageView)
                    title.set(response.body()?.Title)
                    desc.set(response.body()?.Plot)
                    year.set(response.body()?.Year)
                    rating.set("IMDb Rating: " + response.body()?.imdbRating)
                }

                isLoading.set(false)
            }

            override fun onFailure(call: Call<MovieDetails>, t: Throwable)
            {
                TODO("Not yet implemented")
            }

        })
    }
}